package count

import (
	"fmt"
	"time"
)

func Count(seconds int) {
	for i := seconds; i >= 0; i-- {
		fmt.Printf("\r%d \b", i)
		time.Sleep(time.Second)
	}
	fmt.Print("\r")
}
