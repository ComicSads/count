package main

import (
	"fmt"
	"gitlab.com/ComicSads/count/count"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Please give exactly one argument")
		os.Exit(1)
	}
	seconds, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Println("Please provide a number to count from")
	}
	count.Count(seconds)
}
